<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function index(){
        $cast = DB::table('cast')->get();
    
        return view('cast.tampil', ['cast' => $cast]);
    }
    public function create(){
        return view('cast.tambah');
    }
    public function store(Request $request){
        // dd($request->all()); cek form yang dikirim
        $request->validate([
            'nama'=>'required|alpha|min:5',
            'umur'=>'required|numeric',
            'bio'=>'required',
        ]);
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);
        return redirect('/cast');
    }
    public function show($id){
        // dd($request->all()); cek form yang dikirim
        $cast_detail=DB::table('cast')->find($id);
        return view('cast.detail',['cast_detail'=>$cast_detail]);
    }
    public function edit($id){
        // dd($request->all()); cek form yang dikirim
        $cast_detail=DB::table('cast')->find($id);
        return view('cast.edit',['cast_detail'=>$cast_detail]);
    }
    public function update($id, Request $request){
        $request->validate([
            'nama'=>'required|alpha|min:5',
            'umur'=>'required|numeric',
            'bio'=>'required',
        ]);
        DB::table('cast')
            ->where('id',$id)
            ->update(
                [
                    'nama'=>$request->input('nama'),
                    'umur'=>$request->input('umur'),
                    'bio'=>$request->input('bio')
                ]
            )
         ;
        return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')
            ->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
