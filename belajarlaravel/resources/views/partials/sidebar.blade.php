<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="/" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                    {{-- <span class="right badge badge-danger">Dashboard</span> --}}
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="/cast" class="nav-link">
                <i class="nav-icon fas fa-user-tie"></i>
                <p>
                    Cast
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-table"></i>
                <p>
                    Table
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="/table" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Table</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/data-table" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Table</p>
                    </a>
                </li>
            </ul>
        </li>
        
    </ul>
</nav>