
<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<form action="/welcome" method="POST">
        @csrf
		<h1>Buat Account Baru</h1>
		<h3>Sign Up Form</h3>
		<p>First Name:</p>
		<input type="text" autofocus required name="fname" placeholder="First Name" >
		<p>Last Name:</p>
		<input type="text" required name="lname" placeholder="Last Name" >
		<p>Gender:</p>
		<input type="radio" name="" >Male<br>
		<input type="radio" name="" >Female<br>
		<input type="radio" name="" >Other
		<p>Nationally:</p>
		<select name="" >
			<option>Indonesia</option>	
			<option>Palestina</option>	
			<option>Yaman</option>	
		</select>		
		<p>Language Spoken:</p>
		<input type="checkbox" name="" >Bahasa Indonesia<br>
		<input type="checkbox" name="" >English<br>
		<input type="checkbox" name="" >Other
		<p>Bio:</p>
		<textarea placeholder="Bio..." ></textarea>
		<input type="submit" value="Sign Up">	
	</form>
</body>
</html>