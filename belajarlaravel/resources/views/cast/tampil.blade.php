@extends('layouts.master')
@section('title')
Tampil Cast
@endsection
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@push('script')
<script src="{{asset('./template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('./template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@section('content')
<p>
    <a href="/cast/create" class="btn btn-sm btn-primary">Tambah</a>
</p>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $castnya)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$castnya->nama}}</td>
            <td>
                <form action="/cast/{{$castnya->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$castnya->id}}" class="btn btn-info btn-sm"> Detail</a>
                    <a href="/cast/{{$castnya->id}}/edit" class="btn btn-warning btn-sm"> Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="3" class="text-center">Gak ono datane bro</td>
        </tr>
        @endforelse
    </tbody>    
</table>	
@endsection
