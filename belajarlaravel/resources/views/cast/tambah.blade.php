@extends('layouts.master')
@section('title')
Tambah Cast
@endsection

@section('content')

<!-- /.card-header -->
<!-- form start -->
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPassword1">Umur</label>
        <input type="number" class="form-control @error('umur') is-invalid @enderror" placeholder="Umur" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPassword1">Bio</label>
        <textarea class="form-control @error('bio') is-invalid @enderror" name="bio"  cols="30" rows="5"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection
