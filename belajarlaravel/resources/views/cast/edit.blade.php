@extends('layouts.master')
@section('title')
Edit Cast
@endsection

@section('content')

<!-- /.card-header -->
<!-- form start -->
<form action="/cast/{{$cast_detail->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" value="{{$cast_detail->nama}}" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPassword1">Umur</label>
        <input type="number" value="{{$cast_detail->umur}}" class="form-control @error('umur') is-invalid @enderror" placeholder="Umur" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPassword1">Bio</label>
        <textarea class="form-control @error('bio') is-invalid @enderror" name="bio"  cols="30" rows="5">{{$cast_detail->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection
