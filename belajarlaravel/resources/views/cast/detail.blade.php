@extends('layouts.master')
@section('title')
Detail Cast
@endsection
@section('content')
<h1>{{$cast_detail->nama}}</h1>
<p>{{$cast_detail->umur}}</p>
<p>{{$cast_detail->bio}}</p>
<a href="/cast" class="btn btn-sm btn-info"> Kembali</a>
@endsection
